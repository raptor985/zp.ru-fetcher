<#ftl encoding="utf-8">
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="R5">
    <title>Тестовое задание - зарплата.ру</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
	<style>
		h2 {
			text-align: left;
		}
	</style>
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h1>Отчёт на ${date}</h1>
                <h2>
					Топ вакансий по рубрикам
				</h2>
				<table class="table">
					<tr>
						<th>Название рубрики</th>
						<th>Количество вакансий</th>
					</tr>
                                        <#list rubrics?keys as rubric>
                                        <tr>
                                        <td>
                                            ${rubric} 
                                        </td>
                                        <td>
                                            ${rubrics[rubric]}
                                        </td>
                                        </tr>
                                        </#list>
				</table>
				<br>
				<h2>
					Топ слов по упоминанию их в заголовках вакансий
				</h2>
				<table class="table">
					<tr>
						<th>Слово</th>
						<th>Количество вакансий</th>
					</tr>
					<#list words?keys as word>
                                        <tr>
                                        <td>
                                            ${word} 
                                        </td>
                                        <td>
                                            ${words[word]}
                                        </td>
                                        </tr>
                                        </#list>
				</table>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</body>
</html>
