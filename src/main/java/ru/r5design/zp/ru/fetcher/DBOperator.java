package ru.r5design.zp.ru.fetcher;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Класс работы с БД
 */
public enum DBOperator {
    INSTANCE;

    public static DBOperator getInstance() {
        return INSTANCE;
    }

    Connection connection;
    int QUERY_TIMEOUT = 30;

    DBOperator() {
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:VacanciesData.db");
            initializeStructure();
        } catch (SQLException ex) {
            //Вот здесь отлавливать - не есть хорошо
            Logger.getLogger(DBOperator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
    * Удаляет старые таблицы из БД (если они есть) и создаёт новые
    */
    void initializeStructure() throws SQLException {
        Statement statement = connection.createStatement();
        statement.setQueryTimeout(QUERY_TIMEOUT);
        //Таблица с количеством вхождений каждого из слов в список названий вакансий
        String sql = "DROP TABLE IF EXISTS words_count";
        statement.executeUpdate(sql);
        sql = "CREATE TABLE words_count "
                + "(word   VARCHAR(32) PRIMARY KEY     NOT NULL,"
                + " count    INTEGER DEFAULT 1)";
        statement.executeUpdate(sql);
        //Таблица с количеством вакансий в каждой из рубрик
        sql = "DROP TABLE IF EXISTS vacancies_count";
        statement.executeUpdate(sql);
        sql = "CREATE TABLE vacancies_count "
                + "(id   INTEGER PRIMARY KEY     NOT NULL,"
                + " name VARCHAR(32) NOT NULL,"
                + " count INTEGER DEFAULT 1)";
        statement.executeUpdate(sql);
        statement.close();
    }

    
    /**
    * Увеличивает в БД количество появлений слова на единицу
    *
    * @param  word  отдельное слово (возможно с лишними символами) в любом регистре
    */
    void addWordOccurence(String word) throws SQLException {
        //MBTODO: Заменить на вставку пачкой через параметризованный запрос
        String normalizedWord = word.toLowerCase().replaceAll("[^а-яa-z-]", "");
        if (normalizedWord.length() > 0) {
            PreparedStatement statement = connection.prepareStatement("UPDATE words_count SET count=count+1 WHERE word = ?");
            statement.setQueryTimeout(QUERY_TIMEOUT);
            statement.setString(1, normalizedWord);
            //Если слова ещё нет в БД - добавляем его
            if (statement.executeUpdate() == 0) {
                statement = connection.prepareStatement("INSERT INTO words_count (word) VALUES (?)");
                statement.setQueryTimeout(QUERY_TIMEOUT);
                statement.setString(1, normalizedWord);
                statement.executeUpdate();
            }
            statement.close();
        }
    }

    /**
    * Увеличивает в БД количество появлений рубрики на единицу
    *
    * @param  id  идентификатор рубрики из JSON-объекта работы.ру
    * @param  name  наименование рубрики
    */
    void addRubricOccurence(Integer id, String name) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("UPDATE vacancies_count SET count=count+1 WHERE id = ?");
        statement.setQueryTimeout(QUERY_TIMEOUT);
        statement.setInt(1, id);
        //Если рубрики ещё нет в БД - добавляем её
        if (statement.executeUpdate() == 0) {
            statement = connection.prepareStatement("INSERT INTO vacancies_count (id,name) VALUES (?,?)");
            statement.setQueryTimeout(QUERY_TIMEOUT);
            statement.setInt(1, id);
            statement.setString(2, name);
            statement.executeUpdate();
        }
        statement.close();
    }
    
    /**
    * Возвращает полный список рубрик с количеством вакансий в порядке убывания
    */
    Map<String, Integer> getRubricsOccurences() throws SQLException {
        Statement statement = connection.createStatement();
        statement.setQueryTimeout(QUERY_TIMEOUT); 
        String sql = "SELECT name, count FROM vacancies_count ORDER BY count DESC";
        ResultSet rs = statement.executeQuery(sql);
        Map<String, Integer> res = new LinkedHashMap();
        while (rs.next()) {
            String name = rs.getString("name");
            Integer count = rs.getInt("count");
            res.put(name, count);
          }
        statement.close();
        return res;
    }
    
    /**
    * Возвращает полный список слов с количеством вакансий в порядке убывания
    */
    Map<String, Integer> getWordsOccurences() throws SQLException {
        Statement statement = connection.createStatement();
        statement.setQueryTimeout(QUERY_TIMEOUT); 
        String sql = "SELECT word, count FROM words_count ORDER BY count DESC";
        ResultSet rs = statement.executeQuery(sql);
        Map<String, Integer> res = new LinkedHashMap();
        while (rs.next()) {
            String word = rs.getString("word");
            Integer count = rs.getInt("count");
            res.put(word, count);
          }
        statement.close();
        return res;
    }
}
