package ru.r5design.zp.ru.fetcher;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import com.google.gson.JsonParser;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Main {

    static int REST_PAGE_SIZE = 100;
    //city_id=826 - Новосибирск
    //"fields=" сейчас не позволяет получать только заданные поля, поэтому не используется
    static String REST_URL = "http://rabota.ngs.ru/api/v1/vacancies/?period=today&city_id=826&limit=" + REST_PAGE_SIZE;

    DBOperator dbOp = DBOperator.getInstance();

    /**
     * Обрабатывает полученный из API кусок данных
     *
     * @param data поток с ответом сервера на запрос
     */
    boolean processJSONChunk(BufferedReader data) throws SQLException {
        JsonParser parser = new JsonParser();
        JsonObject body = parser.parse(data).getAsJsonObject();
        JsonArray vacancies = body.getAsJsonArray("vacancies");
        //Размер списка вакансий равен нулю когда вакансии по критериям закончились (offset больше кол-ва)
        if (vacancies.size() == 0) {
            return false;
        }
        for (JsonElement vac : vacancies) {
            JsonObject vacObj = vac.getAsJsonObject();
            String header = vacObj.get("header").getAsString();
            //Считаем разделителями между словами символы ' ', ',', '(', ')'
            String[] headerWords = header.split(" |\\,|\\(|\\)");
            //TODO: Проверка на одинаковые слова в заголовке вакансии
            for (String word : headerWords) {
                if (word.length() > 0) {
                    dbOp.addWordOccurence(word);
                }
            }
            JsonArray rubrics = vacObj.getAsJsonArray("rubrics");
            //Судя по JSON-объекту "рубрикой" считается именно рубрика верхнего уровня
            for (JsonElement rubr : rubrics) {
                JsonObject rubrObj = rubr.getAsJsonObject();
                Integer rId = rubrObj.get("id").getAsInt();
                String rName = rubrObj.get("title").getAsString();
                dbOp.addRubricOccurence(rId, rName);
            }
        }
        return true;
    }

    /**
     * Читает записи из API и заполняет БД необходимыми данными
     */
    void fillDB() throws IOException, SQLException {
        //Есть ощутимая вероятность, что прямо в момент обращений к API добавят новые вакансии. В итоге возможна небольшая погрешность.
        HttpClient client = HttpClientBuilder.create().build();
        BufferedReader rd;
        int offset = 0;
        for (boolean forward = true; forward;) {
            HttpGet request = new HttpGet(REST_URL + "&offset=" + offset);
            offset += REST_PAGE_SIZE;
            HttpResponse response = client.execute(request);
            rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            forward = processJSONChunk(rd);
            rd.close();
        }
    }

    /**
     * Собирает отчёт с необходимыми таблицами, помещая данные из БД в шаблон
     */
    void buildReport() throws IOException, SQLException, TemplateException {
        //Заполнение объекта данными таблиц
        Map<String, Object> data = new HashMap<String, Object>();
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date date = new Date();
        data.put("date", dateFormat.format(date));
        data.put("rubrics", dbOp.getRubricsOccurences());
        data.put("words", dbOp.getWordsOccurences());
        //Загрузка шаблона
        Configuration cfg = new Configuration();
        String encoding = "UTF-8";
        cfg.setDefaultEncoding(encoding);
        cfg.setOutputEncoding(encoding);
        cfg.setURLEscapingCharset(encoding);
        Template template = cfg.getTemplate("src/main/resources/tables.ftl");
        // Вывод в файл
        OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream(new File("Отчёт.html")), "UTF-8");
        template.process(data, osw);
        osw.flush();
        osw.close();
    }

    void run() throws IOException, SQLException, TemplateException {
        System.out.println("Filling DB from API...");
        fillDB();
        System.out.println("Building report...");
        buildReport();
        System.out.println("Well done!");
    }

    public static void main(String[] args) throws IOException, SQLException, TemplateException {
        Main app = new Main();
        app.run();
    }
}
